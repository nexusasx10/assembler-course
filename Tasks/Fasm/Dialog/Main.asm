include 'WIN32AX.INC'

message db 'Yes or no?', 0
title db 'Warning!',0
err_mess DB "Critical error!", 0 
norm_mess DB "OK", 0 

start:
  push MB_YESNO
  push title
  push message
  push NULL
  call [MessageBox]
  cmp eax, 6
  jne _err
  push MB_OK + MB_ICONERROR
  push title
  push err_mess
  push NULL
  call [MessageBox]
  jmp _close
_err:
  push MB_OK + MB_ICONINFORMATION
  push title
  push norm_mess
  push NULL
  call [MessageBox]
_close:
  push 0
  call [ExitProcess]
.end start
