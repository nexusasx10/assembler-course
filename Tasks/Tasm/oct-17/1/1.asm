	model tiny
	.code
	org 100h
	locals
start:
	jmp	word ptr uk
m1	db 	'A', 'B', 'C', 23h
m2	dw	'BA', 2343h
m3	db	'ABCD', 0, 0ah, 24h

m4	dd	12345678h
m5	db	78h, 56h, 34h, 12h
m6	dq	123456789ABCDEF0h
m7	dt	12121212121212121212h

metka1:
	mov	ah, 9
	mov	dx, offset mess
	int	21h
	ret
mess db 'Ok!', 0dh, 0ah, '$'
uk dw offset metka1:
end start