	model tiny
	.code
	org 100h
start:
	mov bx, offset number
	add bx, 3
	mov al, byte ptr [offset number]
	mov byte ptr [offset number], 0
	mov cl, 10
re:
	dec bx
	mov ah, 0
	div cl
	mov byte ptr [bx], ah
	add word ptr [bx], 30h
	mov ah, 9h
	cmp al, 0
	jne re
	mov dx, bx
	int 21h
	ret
number db 123, 0, 0, 0dh, 0ah, 24h
end start
