.model tiny
.code
org 100h
.386
locals
start:

call function
ret

proc function
	finit
	fldz
    fld a
	fcom
	fstsw ax
	sahf
	jb g
	fsin
	jmp e
g:
	fcos
	jmp e
e:
	ret
endp
a dd 3.14159265
end start
