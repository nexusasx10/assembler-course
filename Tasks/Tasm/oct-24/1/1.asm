	model	tiny
	.code
	org	100h
	locals
start:
	mov 	byte ptr mess1, 93
	mov	ax, 1
	mov	bx, 2
	mov	ax, bx
	je	@@1
;	
	mov	dx, offset mess2
	jmp	@@2

@@1:
	mov	dx, offset mess1
@@2:
	mov	ah, 9
	int	21h
	ret

mess1	db "Условие выполнено!", 0dh, 0ah, 24h
mess2	db "Условие не выполнено!", 0dh, 0ah, 24h
end start
