 .MODEL tiny
 .CODE
 ORG 100h
 .486

start:
 finit
 mov ax,013h
 int 10h
 call draw
 mov ah, 4ch
 ;mov ah, 7
 int 21h
 ret

 
proc function
 push ax
 fldz
 fcomp
 fstsw ax
 sahf
 jb g
 mov word ptr x, 8
 fild word ptr x
 fdiv
 fsin
 mov word ptr x, 8
 fild word ptr x
 fmul
 jmp e
g:
 fld st
 fmul
 mov word ptr x, 100
 fild word ptr x
 fdiv
 jmp e
e:
 pop ax
 ret
endp


proc draw
 mov ah, 0ch
 mov bh, 1

 mov al, 0fh  ; цвет осей

 ; OX
 mov cx, 319  ; от
 mov dx, 100
setx:
 int 10h
 dec cx
 cmp cx, 0  ; до
 jnz setx

 mov dx, 0
setj:
 mov cx, 162  ; от
seti:
 int 10h
 dec cx
 cmp cx, 157  ; до
 jnz seti
 add dx, 10
 cmp dx, 200
 jnz setj

 ; OY
 mov dx, 199  ; от
 mov cx, 160
sety:
 int 10h
 dec dx
 cmp dx, 0  ; до
 jnz sety

 mov cx, 0
setii:
 mov dx, 102  ; от
setjj:
 int 10h
 dec dx
 cmp dx, 97  ; до
 jnz setjj
 add cx, 10
 cmp cx, 320
 jnz setii

 mov al, 0ah  ; цвет графика
 mov cx, 319  ; длина
sett:
 mov word ptr x, cx
 fild word ptr x
 mov word ptr x, 160  ; смещение по x
 fild word ptr x
 fsub
 call function
 frndint  ; округление
 fchs  ; переворот графика
 fist word ptr x
 mov dx, word ptr x
 cmp dx, 32767
 jb positive  ; если отрицательное - превращаем в положительное
 sub dx, 32767
positive:
 add dx, 100  ; смещение по y
 cmp dx, 199
 jg over  ; фильтрация значений не входящих на экран
 int 10h
over:
 loop sett
 ret
endp

x db ?
END start
